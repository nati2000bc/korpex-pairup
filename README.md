# PairUp! App

### Basic scripts
1. To start project for development run: `yarn start`
1. To build project run `yarn build`
1. To start jest test console run `yarn test`

### Deployment
1. Every push to master branch triggers tests and allows for manual deployment to test environment
