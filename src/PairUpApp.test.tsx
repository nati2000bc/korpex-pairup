import React from 'react';
import { render } from '@testing-library/react';
import PairUpApp from './PairUpApp';

describe('<PairUpApp />', () => {
    it('just renders', () => {
        const { getByText } = render(<PairUpApp />);
        const headerElement = getByText(/Korpex Pair Up!/i);
        expect(headerElement).toBeInTheDocument();
    });
});
