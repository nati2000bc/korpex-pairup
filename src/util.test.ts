import { getAge } from './util';

describe('util', () => {
    it('return zero for this year', () => {
        expect(getAge(new Date())).toBe(0);
    });
    it('returns correct age in simple examples', () => {
        Date.now = jest.fn().mockReturnValue(new Date('May 27, 2000'));
        expect(getAge(Date.parse('Jan 1, 1980'))).toBe(20);
        expect(getAge(Date.parse('May 28, 1980'))).toBe(19);
        expect(getAge(Date.parse('May 26, 1980'))).toBe(20);
    });
    it('returns correct age on birthday', () => {
        Date.now = jest.fn().mockReturnValueOnce(new Date('Aug 9, 2000'));
        expect(getAge(Date.parse('Aug 9, 1995'))).toBe(5);
    });
});
