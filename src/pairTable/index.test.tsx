import { Employee } from '../actions/employees';
import { render, fireEvent } from '@testing-library/react';
import React from 'react';
import { PairTable } from './index';
import { EmployeePair } from '../actions/pairing';

const employeeOne: Employee = {
    id: '1',
    firstName: 'Jessica',
    lastName: 'Jones',
    defect: 'vision',
    birthDate: Date.parse('Jan 10, 1980'),
    team: 'Avengers',
    city: 'Boston',
    district: 'Red',
};
const employeeTwo: Employee = {
    id: '2',
    firstName: 'Iron',
    lastName: 'Man',
    birthDate: Date.parse('Jan 10, 1976'),
    team: 'X-Men',
    city: 'New Yor',
    district: 'Blue',
};

const examplePair: EmployeePair = {
    id: '1_2',
    pair: [employeeTwo, employeeOne],
};

describe('<PairTable />', () => {
    it('Test simple render', () => {
        const { getByText } = render(
            <PairTable onLoadPairs={jest.fn()} pairs={[examplePair]} />
        );
        const names = getByText(/Iron Man\s*Jessica Jones/i);
        expect(names).toBeInTheDocument();
    });

    it('Test button triggers pairing', () => {
        const onLoadPairs = jest.fn();
        const { getByTestId } = render(<PairTable onLoadPairs={onLoadPairs} />);
        const pairButton = getByTestId('pair-button');
        expect(pairButton).toBeVisible();

        fireEvent.click(pairButton);

        expect(onLoadPairs).toBeCalledTimes(1);
    });

    it('Test button is not triggered when disabled', () => {
        const onLoadPairs = jest.fn();
        const { getByTestId } = render(
            <PairTable onLoadPairs={onLoadPairs} disabled />
        );
        const pairButton = getByTestId('pair-button');
        expect(pairButton).toBeVisible();

        fireEvent.click(pairButton);

        expect(onLoadPairs).toBeCalledTimes(0);
    });

    it('Test shows message for unmatched employees', () => {
        const { getByText } = render(
            <PairTable
                onLoadPairs={jest.fn()}
                pairs={[examplePair]}
                unmatchedEmployees={[employeeOne]}
            />
        );
        const pairingError = getByText(
            /we were unable to pair these employees/i
        );
        expect(pairingError).toBeInTheDocument();
    });
});
