import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import { Column, Table } from '../table';
import Box from '@material-ui/core/Box';
import { EmployeePair } from '../actions/pairing';
import { getAge } from '../util';
import { Employee } from '../actions/employees';
import { Typography } from '@material-ui/core';

const renderTwoLine = (
    lineOne: React.ReactNode,
    lineTwo: React.ReactNode,
    defaultValue = ''
) => (
    <>
        {lineOne || defaultValue}
        <br />
        {lineTwo || defaultValue}
    </>
);
const columns: Column<EmployeePair>[] = [
    {
        label: 'Names',
        format: ({ pair: [e1, e2] }) =>
            renderTwoLine(
                `${e1.firstName} ${e1.lastName}`,
                `${e2.firstName} ${e2.lastName}`
            ),
    },
    {
        label: 'City',
        format: ({ pair: [e1, e2] }) => renderTwoLine(e1.city, e2.city),
    },
    {
        label: 'District',
        format: ({ pair: [e1, e2] }) => renderTwoLine(e1.district, e2.district),
    },
    {
        label: 'Team',
        format: ({ pair: [e1, e2] }) => renderTwoLine(e1.team, e2.team),
    },
    {
        label: 'age',
        align: 'right',
        format: ({ pair: [e1, e2] }) =>
            renderTwoLine(getAge(e1.birthDate), getAge(e2.birthDate)),
    },
    {
        label: 'Defect',
        format: ({ pair: [e1, e2] }) =>
            renderTwoLine(e1.defect, e2.defect, 'None'),
    },
];

interface PairTableProps {
    pairs?: EmployeePair[];
    unmatchedEmployees?: Employee[];
    onLoadPairs: () => void;
    disabled?: boolean;
}

export const PairTable: React.FC<PairTableProps> = ({
    pairs = [],
    unmatchedEmployees,
    onLoadPairs,
    disabled = false,
}) => {
    return (
        <Box p={2}>
            <Card>
                <CardHeader
                    title="Pairs"
                    subheader="Paired up employees for integration party"
                    action={
                        <span>
                            <Tooltip
                                title="Pair up employees!"
                                placement="left"
                            >
                                <IconButton
                                    data-testid="pair-button"
                                    disabled={disabled}
                                    onClick={onLoadPairs}
                                >
                                    <EmojiPeopleIcon />
                                </IconButton>
                            </Tooltip>
                        </span>
                    }
                />
                <Table<EmployeePair> data={pairs} columns={columns} />
                {unmatchedEmployees !== undefined &&
                    unmatchedEmployees.length > 0 && (
                        <Typography variant="body1">
                            Based on pairing restrictions, we were unable to
                            pair these employees:
                            {unmatchedEmployees
                                .map((e) => `${e.firstName} ${e.lastName}`)
                                .join(', ')}
                        </Typography>
                    )}
            </Card>
        </Box>
    );
};
