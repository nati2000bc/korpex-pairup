import React from 'react';
import Box from '@material-ui/core/Box';
import CardHeader from '@material-ui/core/CardHeader';
import Card from '@material-ui/core/Card';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import GetAppIcon from '@material-ui/icons/GetApp';
import { Employee } from '../actions/employees';
import { Column, Table } from '../table';
import { getAge } from '../util';

const columns: Column<Employee>[] = [
    {
        propKey: 'firstName',
        label: 'First Name',
    },
    {
        propKey: 'lastName',
        label: 'Last Name',
    },
    {
        propKey: 'city',
        label: 'City',
    },
    {
        propKey: 'district',
        label: 'District',
    },
    {
        propKey: 'team',
        label: 'Team',
    },
    {
        label: 'age',
        align: 'right',
        format: (employee) => getAge(employee.birthDate),
    },
    {
        label: 'Defect',
        format: (employee) => employee.defect || 'None',
    },
];

interface EmployeeTableProps {
    employees: Employee[];
    onLoadEmployees: () => void;
}

export const EmployeeTable: React.FC<EmployeeTableProps> = ({
    employees,
    onLoadEmployees,
}) => {
    return (
        <Box p={2}>
            <Card>
                <CardHeader
                    title="List of Employees"
                    subheader="List of all employees at Korpex that are going to integration party"
                    action={
                        <Tooltip
                            title="Load random employee list"
                            placement="left"
                        >
                            <IconButton
                                data-testid="table-load"
                                onClick={onLoadEmployees}
                            >
                                <GetAppIcon />
                            </IconButton>
                        </Tooltip>
                    }
                />
                <Table<Employee> data={employees} columns={columns} />
            </Card>
        </Box>
    );
};
