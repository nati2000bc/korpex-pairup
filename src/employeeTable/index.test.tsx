import { render, fireEvent } from '@testing-library/react';
import React from 'react';
import { EmployeeTable } from './index';
import { Employee } from '../actions/employees';

describe('<EmployeeTable />', () => {
    it('just renders', () => {
        const { getByText } = render(
            <EmployeeTable employees={[]} onLoadEmployees={() => undefined} />
        );
        const headerElement = getByText(/List of Employees/i);
        expect(headerElement).toBeInTheDocument();
    });

    it('Click on icon triggers load', () => {
        const mockLoad = jest.fn();
        const { getByTestId } = render(
            <EmployeeTable employees={[]} onLoadEmployees={mockLoad} />
        );
        const button = getByTestId('table-load');
        expect(button).toBeVisible();

        expect(mockLoad).toBeCalledTimes(0);
        fireEvent.click(button);
        expect(mockLoad).toBeCalledTimes(1);
    });

    it('Renders a single employee', () => {
        const testEmployee: Employee = {
            id: '123',
            firstName: 'Jessica',
            lastName: 'Jones',
            defect: 'vision',
            birthDate: new Date(),
            team: 'Avengers',
            city: 'Boston',
            district: 'Red',
        };

        const { getByText } = render(
            <EmployeeTable
                employees={[testEmployee]}
                onLoadEmployees={() => undefined}
            />
        );

        expect(getByText(/Jessica/)).toBeVisible();
        expect(getByText(/Jones/)).toBeVisible();
        expect(getByText(/vision/)).toBeVisible();
        expect(getByText(/Avengers/)).toBeVisible();
        expect(getByText(/Boston/)).toBeVisible();
        expect(getByText(/Red/)).toBeVisible();
    });
});
