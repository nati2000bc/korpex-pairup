import React, { useState, ReactNode } from 'react';
import { default as MUITable } from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';

export type DataItem = { id: string; [key: string]: any };

interface ColumnBasicProps {
    align?: 'left' | 'right';
    label: string;
}

interface SimpleColumn<T extends DataItem> extends ColumnBasicProps {
    propKey: keyof T;
}

interface ComplexColumn<T extends DataItem> extends ColumnBasicProps {
    format: (value: T) => ReactNode;
}

export type Column<T extends DataItem> = SimpleColumn<T> | ComplexColumn<T>;

interface TableProps<T extends DataItem> {
    data: T[];
    columns: Column<T>[];
}

const renderRow: <T extends DataItem>(
    element: T,
    columns: Column<T>[]
) => React.ReactNode = (element, columns) =>
    columns.map((column, index) => (
        <TableCell key={index} align={column.align}>
            {'propKey' in column
                ? element[column.propKey]
                : column.format(element)}
        </TableCell>
    ));

export const Table: <T extends DataItem>(
    props: TableProps<T>
) => React.ReactElement<TableProps<T>> = ({ data, columns }) => {
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    return (
        <>
            <TableContainer>
                <MUITable stickyHeader>
                    <TableHead>
                        <TableRow>
                            {columns.map((column, index) => (
                                <TableCell key={index} align={column.align}>
                                    {column.label}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data
                            .slice(
                                page * rowsPerPage,
                                page * rowsPerPage + rowsPerPage
                            )
                            .map((item) => (
                                <TableRow key={item.id}>
                                    {renderRow(item, columns)}
                                </TableRow>
                            ))}
                        <TableRow>
                            <TablePagination
                                rowsPerPageOptions={[10, 25, 100]}
                                count={data.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                            />
                        </TableRow>
                    </TableBody>
                </MUITable>
            </TableContainer>
        </>
    );
};
