// pure JS way of calculating age...
export const getAge = (birthDate: Date | number) => {
    return new Date(+Date.now() - +new Date(birthDate)).getFullYear() - 1970;
};
