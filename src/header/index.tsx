import React from 'react';
import { AppBar, Toolbar, Typography } from '@material-ui/core';

export const Header: React.FC = () => {
    return (
        <AppBar position="sticky" color="default">
            <Toolbar>
                <Typography variant="h6" color="inherit" noWrap>
                    Korpex Pair Up!
                </Typography>
            </Toolbar>
        </AppBar>
    );
};
