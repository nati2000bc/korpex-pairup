import React from 'react';
import { render } from '@testing-library/react';
import { Header } from './index';

describe('<Header />', () => {
    it('just renders', () => {
        const { getByText } = render(<Header />);
        const headerElement = getByText(/Korpex/i);
        expect(headerElement).toBeInTheDocument();
    });
});
