import React, { useState } from 'react';
import { Header } from './header';
import Container from '@material-ui/core/Container';
import { EmployeeTable } from './employeeTable';
import { Employee, loadRandomEmployees } from './actions/employees';
import { pairEmployees, PairingResult } from './actions/pairing';
import { PairTable } from './pairTable';

const PairUpApp: React.FC = () => {
    const [employees, setEmployees] = useState<Employee[]>([]);
    const [pairing, setPairing] = useState<PairingResult>();
    const onLoadEmployees = () => {
        setEmployees(loadRandomEmployees());
        setPairing(undefined);
    };
    const onLoadPairs = () => {
        if (employees.length > 0) {
            setPairing(pairEmployees(employees));
        }
    };

    return (
        <>
            <Header />
            <Container component="main" maxWidth="md">
                <EmployeeTable
                    employees={employees}
                    onLoadEmployees={onLoadEmployees}
                />
                <PairTable
                    disabled={employees.length <= 0}
                    pairs={pairing?.pairs}
                    onLoadPairs={onLoadPairs}
                />
            </Container>
        </>
    );
};

export default PairUpApp;
