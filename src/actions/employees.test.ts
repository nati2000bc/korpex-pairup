import { loadRandomEmployees } from './employees';

describe('Test emplyoees logic', () => {
    it('check default number of employees render', () => {
        let employees = loadRandomEmployees();
        expect(employees).toHaveLength(100);
    });
});
