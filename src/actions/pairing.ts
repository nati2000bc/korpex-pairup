import { Employee } from './employees';
import { getAge } from '../util';
import { hopcroftKarp, HopcroftKarpInput } from 'hopcroft-karp';

export interface EmployeePair {
    id: string;
    pair: [Employee, Employee];
}

export const canEmployeesBeMatched = (a: Employee, b: Employee) => {
    if (a.team === b.team) {
        return false;
    }
    if (getAge(a.birthDate) === getAge(b.birthDate)) {
        return false;
    }
    if (a.defect === 'vision' && b.defect === 'vision') {
        return false;
    }
    if (a.city === b.city && a.district === b.district) {
        return false;
    }
    return true;
};

export type PairResult = {
    pairs: EmployeePair[];
    unmatched: Employee[];
};

export type PairingResult = {
    unmatched: Employee[];
    pairs: EmployeePair[];
};

export const pairEmployees = (employees: Employee[]): PairingResult => {
    const employeeMap = employees.reduce((map, employee) => {
        map[employee.id] = employee;
        return map;
    }, {} as { [id: string]: Employee | null });

    const input: HopcroftKarpInput = employees.reduce((map, employee) => {
        map[employee.id] = employees
            .filter((e) => canEmployeesBeMatched(employee, e))
            .map((e) => e.id);
        return map;
    }, {} as HopcroftKarpInput);

    const karpResult = hopcroftKarp(input);
    const pairs = Object.entries(karpResult)
        .map(([key, value]) => {
            if (!employeeMap[key] || !employeeMap[value]) {
                return null;
            }
            const pair: [Employee, Employee] = [
                employeeMap[key]!,
                employeeMap[value]!,
            ];
            employeeMap[key] = null;
            employeeMap[value] = null;
            return {
                id: pair[0].id + '_' + pair[1].id,
                pair,
            };
        })
        .filter((pair) => pair !== null) as EmployeePair[];
    const unmatched = Object.values(employeeMap).filter(
        (e) => e !== null
    ) as Employee[];
    return {
        pairs,
        unmatched,
    };
};
