declare module 'hopcroft-karp' {
    export interface HopcroftKarpInput {
        [key: string]: string[];
    }
    export interface HopcroftKarpResult {
        [key: string]: string;
    }
    export function hopcroftKarp(input: HopcroftKarpInput): HopcroftKarpResult;
}
