import { canEmployeesBeMatched, pairEmployees } from './pairing';
import { Employee } from './employees';

const employeeOne: Employee = {
    id: '1',
    firstName: 'Jessica',
    lastName: 'Jones',
    defect: 'vision',
    birthDate: Date.parse('Jan 10, 1980'),
    team: 'Avengers',
    city: 'Boston',
    district: 'Red',
};
const employeeTwo: Employee = {
    id: '2',
    firstName: 'Iron',
    lastName: 'Man',
    birthDate: Date.parse('Jan 10, 1976'),
    team: 'X-Men',
    city: 'New Yor',
    district: 'Blue',
};
const employeeThree: Employee = {
    id: '3',
    firstName: 'Jan',
    lastName: 'Kowalski',
    birthDate: Date.parse('Jan 10, 1970'),
    team: 'Randevous',
    city: 'Atlanta',
    district: 'Yellow',
};

const employeeFour: Employee = {
    id: '4',
    firstName: 'Anna',
    lastName: 'Kowalski',
    birthDate: Date.parse('Jan 10, 1990'),
    team: 'Foxtrot',
    city: 'San Diego',
    district: 'Black',
};
describe('employee check', () => {
    it('Check not matching to itself', () => {
        expect(canEmployeesBeMatched(employeeOne, employeeOne)).toBe(false);
        expect(canEmployeesBeMatched(employeeOne, { ...employeeOne })).toBe(
            false
        );
    });
    it('Check not matching scenarios', () => {
        expect(
            canEmployeesBeMatched(employeeOne, {
                ...employeeTwo,
                city: employeeOne.city,
                district: employeeOne.district,
            })
        ).toBe(false);

        expect(
            canEmployeesBeMatched(employeeOne, {
                ...employeeTwo,
                birthDate: Date.parse('May 27, 1980'),
            })
        ).toBe(false);

        expect(
            canEmployeesBeMatched(employeeOne, {
                ...employeeTwo,
                team: employeeOne.team,
            })
        ).toBe(false);

        expect(
            canEmployeesBeMatched(employeeOne, {
                ...employeeTwo,
                defect: employeeOne.defect,
            })
        ).toBe(false);
    });
    it('Check matching scenarios', () => {
        expect(canEmployeesBeMatched(employeeOne, employeeTwo)).toBe(true);
        expect(
            canEmployeesBeMatched(employeeOne, {
                ...employeeTwo,
                firstName: employeeOne.firstName,
                lastName: employeeOne.lastName,
            })
        ).toBe(true);
        expect(
            canEmployeesBeMatched(employeeOne, {
                ...employeeTwo,
                city: employeeOne.city,
            })
        ).toBe(true);
        expect(
            canEmployeesBeMatched(employeeOne, {
                ...employeeTwo,
                // same district but different city
                district: employeeOne.district,
            })
        ).toBe(true);
    });
});

describe('Test matching algorithm', () => {
    it('Test simple scenario', () => {
        const result = pairEmployees([employeeOne, employeeTwo]);
        expect(result.pairs).toHaveLength(1);
        expect(result.unmatched).toHaveLength(0);
        expect(result.pairs[0].pair).toContain(employeeOne);
        expect(result.pairs[0].pair).toContain(employeeTwo);
    });

    it('Test only one employee', () => {
        const result = pairEmployees([employeeOne]);
        expect(result.pairs).toHaveLength(0);
        expect(result.unmatched).toStrictEqual([employeeOne]);
    });

    it('Test odd number of employees', () => {
        const result = pairEmployees([employeeOne, employeeTwo, employeeThree]);
        expect(result.pairs).toHaveLength(1);
        expect(result.unmatched).toHaveLength(1);
    });

    it('Test not matching employees', () => {
        const result = pairEmployees([
            employeeOne,
            { ...employeeTwo, team: employeeOne.team },
        ]);
        expect(result.pairs).toHaveLength(0);
        expect(result.unmatched).toHaveLength(2);
    });

    it('Test two pair scenario, more difficult', () => {
        const result = pairEmployees([
            { ...employeeOne, defect: undefined },
            { ...employeeThree, defect: 'vision' },
            { ...employeeTwo, defect: undefined },
            { ...employeeFour, defect: 'vision' },
        ]);
        expect(result.pairs).toHaveLength(2);
        expect(result.unmatched).toHaveLength(0);

        let [firstPairEmployeeOne, firstPairEmployeeTwo] = result.pairs[0].pair;
        expect(firstPairEmployeeOne.defect !== firstPairEmployeeTwo.defect);
    });

    it('4 people one pair', () => {
        const result = pairEmployees([
            { ...employeeOne, defect: 'vision' },
            { ...employeeThree, defect: 'vision' },
            { ...employeeTwo, defect: undefined },
            { ...employeeFour, defect: 'vision' },
        ]);
        expect(result.pairs).toHaveLength(1);
        expect(result.unmatched).toHaveLength(2);
    });
});
