import faker from 'faker';

export interface Employee {
    id: string;
    firstName: string;
    lastName: string;
    city: string;
    district: string;
    team: string;
    birthDate: Date | number;
    defect?: 'vision';
}

const cities = new Array(3).fill('').map(() => faker.address.city());
const districts = new Array(5).fill('').map(() => faker.address.county());
const teams = ['Avengers', 'Fantastic Four', 'X-Men', 'Ninjas', 'Samurais'];
const youngestEmployeeBirthDate = new Date();
youngestEmployeeBirthDate.setFullYear(
    youngestEmployeeBirthDate.getFullYear() - 20
);

export const loadRandomEmployees = (numberOfEmployees = 100) =>
    new Array(numberOfEmployees).fill('').map(createRandomEmployee);
const createRandomEmployee = (): Employee => {
    const birthDate = faker.date.past(30, youngestEmployeeBirthDate);
    return {
        id: faker.random.uuid(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        city: cities[faker.random.number(cities.length - 1)],
        district: districts[faker.random.number(districts.length - 1)],
        team: teams[faker.random.number(teams.length - 1)],
        birthDate: birthDate,
        defect: faker.random.number(100) < 30 ? 'vision' : undefined, //give it 30% chance
    };
};
